# Mile App Assignment
**Do you prefer vuejs or reactjs? Why ?**

It depends based on project demand. Both have pros & cons. Let me share some ideas for both.

**Syntax:**

**Vue:** You can use HTML or JSX or createElement in render functions

**React:** You can use only JSX and React.createElement methods

**Api Surface**

React has a very small API surface. Things I regularly import: Fragment, createContext, Hooks (useState, useEffect, useContext,…) On the other hand, Vue has a vast amount of component options, instance properties, and template directives to remember. I prefer React’s modest amount of building blocks

**Functions over classes**

The majority of components I write are essentially functions that receive props and return a view. React promotes the use of function components, so there’s no syntax overhead of using classes or objects. No more reasoning about the this keyword either, simply props => view.
Fragments

In React, components don’t have the limitation of having to return exactly one DOM node. You can return a list of nodes, or just text wrapped in a fragment.
so both have pros and cons in terms of usability.

**What complex things have you done in frontend development ?**

- Develop payment interface so that merchant and corporate client integrate that in their system and get payments for customer
- Centralized step controller for application so that you can control whole application routes from a single point

**why does a UI Developer need to know and understand UX? how far do you understand it?**

- The front end connects a human being to a computer application.As a developer, you understand the “computer application” part.You need to learn UX to understand the “human being” part.Otherwise, you will never understand more than 50% of the requirements for your job.

**Give your analysis results regarding https://taskdev.mile.app/login from the UI / UX side!**
- Can be added Topbar & Footer.
- Organization name input length validation needed.
- As input field mandatory so Login button should be disabled by default

**Create a better login page based on https://taskdev.mile.app/login and deploy on https://www.netlify.com (https://www.netlify.com/)!**

https://focused-bose-75b746.netlify.app/

**6.a Swap the values of variables A and B**
```
Destructuring assignment
let A = 3
let B = 5
console.log([A, B] = [B, A]);

Bitwise XOR operator
let a = 3;
let b = 5;

a = a ^ b;
b = a ^ b;
a = a ^ b;
console.log(a);
console.log(b);
```

**6.b Find the missing numbers from 1 to 100**
```
var numbers = [1, 2, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100],
count = 100;
var missing = new Array();

for (var i = 1; i <= count; i++) {
  if (numbers.indexOf(i) == -1) {
    missing.push(i);
  }
}
console.log(missing); 
```

**6.c return the number which is called more than 1**
```
const findDuplicates = (arr) => {
  let sorted_arr = arr.slice().sort();
  let duplicateArray = [];
  for (let i = 0; i < sorted_arr.length - 1; i++) {
    if (sorted_arr[i + 1] == sorted_arr[i]) {
      duplicateArray.push(sorted_arr[i]);
    }
  }
  return duplicateArray;
}

let numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 25, 25, 25, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 34, 34, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 83, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 92, 93, 94, 95, 96, 97, 98, 99, 100];
console.log(findDuplicates(numbers));
```

**6.d Return Object**

```
// Input.
const input = ["1.", "1.1.", "1.2.", "1.3.", "1.4.", "1.1.1.", 
               "1.1.2.", "1.1.3.", "1.2.1.", "1.2.2.", "1.3.1.", 
               "1.3.2.", "1.3.3.", "1.3.4.", "1.4.1.", "1.4.3."]

const group = (array) => array.reduce((o, x) => {
  const obj ={}
  const key = x.match(/\d+\.\d+/)
  if (o[key]) obj[key] = x
  else obj[key] = [x]
  return obj
}, {})
const output = group(input)
console.log(output)
```












